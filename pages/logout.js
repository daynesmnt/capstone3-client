import {useContext, useEffect } from 'react'

import UserContext from '../UserContext'
import Router from 'next/router'

export default function index(){

	// consume the userContext

	const { unsetUser } = useContext(UserContext)

	// use useEffect() s that our statement will run once the page renders

	useEffect(()=>{

		unsetUser();
		Router.push('/login')
	},[])

	return null
}