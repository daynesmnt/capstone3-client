import {useState,useEffect} from 'react';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Swal from 'sweetalert2';


import Router from 'next/router';

import Head from 'next/head';

export default function register(){
	//states for our input. user's email and password only

	const [email,setEmail] = useState('')
	const [password1 , setPassword1] = useState('')
	const [password2 , setPassword2] = useState('')

	// validation form input with conditional rendering
	const [isActive,setIsActive] = useState('')
	//form validation wc conditionally renders our submit button
	useEffect(()=>{
		if((password1 !== '' && password2 !== '' && email !== '') && (password1 == password2)){
			setIsActive(true)

		}else{
			setIsActive(false)

		}
	},[password1,password2])


	function register(e){
		e.preventDefault()
		//fetch request to register our user. but first, lets check if the DB has the same email
		fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/email-exists`,{
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email:email

			})
		})
		.then(res => res.json())
		.then(data => {

			//console.log(data)
			//this shall return data as tru/false true if email exist false otherwise
			if(data===false){
				fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users`,{
					method: 'POST',
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						email:email,
						password:password1
					})
				}).then(res => res.json())
				.then(data => {
					//console.log(data)
					if(data === true){
						//console.log('reg success')
						Swal.fire({
						icon:'success',
						title:'Thank you for registering',
						text:'You can now log in'
						})
						Router.push('/login')
					}else{
						//console.log('reg failed')
						Swal.fire({
						icon:'error',
						title:'Something went wrong'
						})
					
						Router.push('/register')
					}
					
				})

			}else{
				Swal.fire({
						icon:'error',
						title:'Try using a different Email'
						})
				}
		})
	}
	return(
		<>
		<Head><title> Register || Vinti</title></Head>
		<h1 className="infoTitle">Register</h1>
		<Form onSubmit={e => register(e)}>
			<Form.Group controlId="email">
				<Form.Label className="infoText">Email Address : </Form.Label>
				<Form.Control type="email" placeholder='Enter Email' value={email} onChange={e => setEmail(e.target.value)} required/>
			</Form.Group>
			<Form.Group controlId="password1">
				<Form.Label className="infoText">Password : </Form.Label>
				<Form.Control type="password" placeholder='Enter password' value={password1} onChange={e => setPassword1(e.target.value)} required/>
			</Form.Group>
			<Form.Group controlId="password2">
				<Form.Label className="infoText">Verify Password : </Form.Label>
				<Form.Control type="password" placeholder='Verify password' value={password2} onChange={e => setPassword2(e.target.value)} required/>
			</Form.Group>
			{
				isActive
				? <Button variant="secondary" type="submit">Submit</Button>
				:
					<Button variant="secondary" type="submit" disabled>Submit</Button>
			}
		</Form>
		</>

		)
}