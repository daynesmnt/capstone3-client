import {useEffect , useState, useContext,Fragment} from 'react';
import { Row } from 'react-bootstrap';
import { Col } from 'react-bootstrap';
import Table from 'react-bootstrap/Table';
import Form from 'react-bootstrap/Form';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import Dropdown from "react-bootstrap/Dropdown";
import DropdownButton from "react-bootstrap/DropdownButton";
import Swal from 'sweetalert2';
import Router from 'next/router';
import UserContext from '../UserContext';

import Alert from 'react-bootstrap/Alert';


import Head from 'next/head';
export default function categories(){
	//const token = localStorage.getItem("token")
	const { user } = useContext(UserContext)
	//console.log(user)
	const [records, setRecords] = useState([])
	const [categoryName,setCategoryName] = useState('')
	const [type, setType] =useState('')
	const [ show, setShow] = useState(false)
	const handleClose =() => setShow(false)
	const handleShow =()=> {
		setShow(true)
	}
	const [isActive ,setIsActive] = useState(false)

	useEffect(()=>{

		
		if(categoryName !== '' && type !== '') {

			setIsActive(true)

		}else {
			setIsActive(false)

		}

	}, [categoryName,type])

	function addCategory(e){
		//console.log('this function will run')
		e.preventDefault()
		fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/newCategory`,{

			method: 'POST',
			headers: {
			'Content-Type' : 'application/json',
			'Authorization' : `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
			
				name: categoryName,
				type: type
			})
		})
		.then(res => {
			return res.json()
		})
		.then( data => {
		//console.log(data)
			if(data === true){
				//console.log('Successful editing')
					Swal.fire({
						icon:'success',
						title:'Category Added'
					})
				Router.push('/categories')
				setShow(false)
				setCategoryName('')
				setType('')
				}else{
				Swal.fire({
						icon:'error',
						title:'Something went wrong'
					})
						setShow(false)
				}
		})

	}
	function income(i){	
		//console.log('this will be income')
		setType('Income')
	}
	function expense(x){	
		//console.log('this will be expense')
		setType('Expense')
	}
	
//console.log(records)






	useEffect(() => {
		fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/details`,{
			headers:{

				'Authorization' : `Bearer ${localStorage.getItem('token')}`
			}
		}).then(res => res.json())
		.then(data => {
			//console.log(data)
			setRecords(data.categories)

		})
	},[records])

	records.map(categories =>{
		//console.log(categories.name)
		//console.log(categories.type)
	})
	return(
		<>
		<Head><title>Categories || Vinti</title></Head>

		
		<Row>

		<Col xs={12} lg={10}>
		<h1 className="text-center" className="infoTitle">My Categories</h1>
			<Button variant="secondary" className="mb-3" onClick={()=>{handleShow()}}>add a category</Button>
		{
			records.length > 0
			?
				<Table striped bordered hover responsive>
					<thead>
						<tr>
							<th>Name</th>
							<th>Type</th>
						</tr>
					</thead>
					<tbody>
						{
							records.map(record => {
								return(
									<tr key={record._id}>
										<td>{record.name}</td>
										<td>{record.type}</td>
									</tr>

									)
							})
						}
					</tbody>		
				</Table>
			:
			<Alert variant="secondary">
				You have no category yet
			</Alert>


		}
		</Col>
		</Row>

			<Modal show={show} onHide={handleClose}>
				<Modal.Header>
					<Modal.Title>Add a Category</Modal.Title>					
				</Modal.Header>
				<Modal.Body>

				<Form onSubmit={(e)=> addCategory(e)}>
					<Form.Group controlId="categoryName">
						<Form.Label>Category Name:</Form.Label>
						<Form.Control type='text' placeholder="Enter Category Name" value={categoryName} onChange={e => setCategoryName(e.target.value)} required />
					</Form.Group>
					<Form.Group controlId="type">
					<DropdownButton variant="secondary" title="Dropdown button" alignRight>
        				<Dropdown.Header>Category Type</Dropdown.Header>
        					<Dropdown.Item onClick={(i)=> income()}>Income</Dropdown.Item>
        					<Dropdown.Item onClick={(x)=> expense()}>Expense</Dropdown.Item>
      				</DropdownButton>
      				<Form.Control type='text' placeholder={type} value={type} required disabled />
						
					</Form.Group>

					{
						isActive === true

						?
						<Button variant="secondary" type="submit" id="submitBtn">Submit</Button>
						:
						<Button variant="secondary" type="submit" id="submitBtn" disabled>Submit</Button>
					}

					
					
				</Form>
			</Modal.Body>
			</Modal>

	
		</>
	
		
		

		)
		
}
