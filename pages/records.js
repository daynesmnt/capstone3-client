import {useEffect , useState, useContext, Fragment} from 'react';
import { Row } from 'react-bootstrap';
import { Col } from 'react-bootstrap';
import Table from 'react-bootstrap/Table';
import Form from 'react-bootstrap/Form';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import Dropdown from "react-bootstrap/Dropdown";
import DropdownButton from "react-bootstrap/DropdownButton";
import Swal from 'sweetalert2';
import Router from 'next/router';
import Card from 'react-bootstrap/Card';
import moment from 'moment';
import {Container} from 'react-bootstrap';



import Alert from 'react-bootstrap/Alert';
import UserContext from '../UserContext' ;

//import RecordList from '../components/RecordList'



import Head from 'next/head';
export default function records(){
	const { user } = useContext(UserContext)
	//console.log(user)
	//records are categories
	const [records, setRecords] = useState([])
	//all wallet is all records in wallet object wether income or expense mixed up
	const [allWallet,setAllWallet] = useState([])

	const [allData,setAllData]=useState([])

	
	const [targetRecord ,setTargetRecord] = useState('')
	const [categoryRecords,setCategoryRecords] = useState([])
	const [categoryId ,setCategoryId] =useState('')
	const [categoryName,setCategoryName] = useState('')
	const [type, setType] =useState('')
	const [amount,setAmount]=useState(0)
	const [description,setDescription]=useState('')


	//states to display records
	const [displayName,setDisplayName] = useState('')
	const [displayType,setDisplayType] = useState('')
	const [displayDesc,setDisplayDesc] = useState('')
	const [displayAmount,setDisplayAmount] = useState(0)


	const [ show, setShow] = useState(false)
	const handleClose =() => setShow(false)
	const handleShow =()=> {
		
		setShow(true)
	}

	const [ showEdit, setShowEdit] = useState(false)
	const handleCloseEdit =() => setShowEdit(false)
	const handleShowEdit =(walletId)=> {
		console.log(walletId)
		setShowEdit(true)
		setWalletId(walletId)
	}
	const [walletId,setWalletId] = useState('')
	const [isActive ,setIsActive] = useState(false)

	useEffect(()=>{

		
		if(categoryId !== '' && type !== '' && amount !== 0 && description !== '') {

			setIsActive(true)

		}else {
			setIsActive(false)

		}

	}, [categoryId,type,amount,description])
		
	// TO GET CATEGORY RECORDS FOR DROPDOWN AND SET CATEGORY NAME AND TYPE FOR RECORDS



	useEffect(() => {
		fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/details`,{
			headers:{

				'Authorization' : `Bearer ${localStorage.getItem('token')}`
			}
		}).then(res => res.json())
		.then(data => {
			//console.log(data)
			setAllWallet(data.wallet)
			setRecords(data.categories)
			setAllData(data)
		})
	},[user.id,allWallet])
	//console.log(allWallet)


	//console.log(records)

/*	useEffect(()=>{
		fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/categories`,{
		headers: {
			'Content-Type' : 'application/json'	
			}
		}).then(res =>{
			return res.json()
		}).then(data=>{
			//console.log(data)
			setCategoryRecords(data)
		})
	},[])*/

// TO SET CATEGORY TYPE
	function income(i){	
		//console.log('this will be income')
		setType('Income')
	}
	function expense(x){	
		//console.log('this will be expense')
		setType('Expense')
	}

//function to add a record
function addRecord(e){
	e.preventDefault()

fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/newWallet`,{
	method: "POST",
	headers:{
		"Content-Type":"application/json",
		'Authorization' : `Bearer ${localStorage.getItem('token')}`
	},
	body:JSON.stringify({
		
		categoryName: categoryName,
		amount: amount,
		description:description,
		type:type,
	})
}).then(res =>{
	return res.json()
}).then(data =>{
	//console.log(data)
	if(data === true){
	//console.log('Record Added')
		Swal.fire({
		icon:'success',
		title:'Successfuly Added'
		})
	Router.push('/records')
	setCategoryName('')
	setAmount(0)
	setDescription('')
	setShow(false)
	}else{
		Swal.fire({
		icon:'error',
		title:'Something went wrong. Please Try Again'
		})
	setShow(false)
	}

})
}

	const allIncomeRecords = allWallet.filter(function(element){
		return element.type == "Income";
	})
	const allExpenseRecords = allWallet.filter(function(element){
		return element.type == "Expense";
	})

	//console.log(allExpenseRecords)
	//console.log(allInomeRecords)


//this is an array of all

	//console.log(incomeTotal)
	//console.log(incomeSum)

	const expenseTotal = allExpenseRecords.map(element =>{
		return element.amount
	})
	const incomeTotal = allIncomeRecords.map(element =>{
		return element.amount
	})

	const expenseSum =expenseTotal.reduce(function(a,b){
		return a+b;
	},0);
	const incomeSum =incomeTotal.reduce(function(a,b){
		return a+b;
	},0);
	//console.log(expenseTotal)
	//console.log(expenseSum)

	let currentBalance = incomeSum - expenseSum

	//console.log(currentBalance)
const [match,setMatch] =useState([])
function search(e){
	
	e.preventDefault()
	//console.log("this will run")
	//console.log(allData.wallet)
	const wallets = allData.wallet
	//console.log(wallets)
	const match = wallets.filter(wallet => wallet.categoryName === targetRecord || wallet.type === targetRecord)
	setMatch(match)
	//console.log(match)

}
	
	function selectIncome(e){

	e.preventDefault()
	//console.log("this will run")
	//console.log(allData.wallet)
	const wallets = allData.wallet
	//console.log(wallets)
	const match = wallets.filter(wallet => wallet.type === "Income")
	setMatch(match)
	setTargetRecord("Income")

	}
			
	function selectExpense(e){

	e.preventDefault()
	//console.log("this will run")
	//console.log(allData.wallet)
	const wallets = allData.wallet
	//console.log(wallets)
	const match = wallets.filter(wallet => wallet.type === "Expense")
	setMatch(match)
	setTargetRecord("Expense")
	}
	
	function editWallet(e){
		e.preventDefault()
		//console.log("this will run")
		fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/${walletId}`,{
			method:"POST",
			headers:{
				"Content-Type": "application/json",
				'Authorization' : `Bearer ${localStorage.getItem('token')}`
			},
			body:JSON.stringify({
				userId:user.id,
				walletId:walletId,
				categoryName:categoryName,
				amount:amount,
				description:description,
				type:type
			})
		}).then(res => res.json())
		.then(data => {
			console.log(data)

		})

	}
		
return(
		<Fragment>
		<Head><title>Records || Vinti</title></Head>
		
		<Row>

		<Col xs={12} lg={10}>
		<h1 className="infoTitle">My Records</h1>
		<Button className="mb-2 mr-2" variant="secondary" onClick={()=>{handleShow()}}> Add a Record</Button>
		
			<Form.Group controlId="targetRecord" className='d-flex' >
			 	<Form.Control type='text' placeholder='Search for a Category or a Type' value={targetRecord} onChange={e => setTargetRecord(e.target.value)}/>
			 	{
			 		targetRecord === ""
			 		?
			 		<Button variant="secondary" type="submit" onClick={(e)=> search(e)} disabled>Search</Button>
			 		:
			 		<Button variant="secondary" type="submit" onClick={(e)=> search(e)}>Search</Button>

			 	}

			 	<DropdownButton variant="secondary" title="Choose a Category" alignRight>
        				<Dropdown.Header>Categories</Dropdown.Header>
        					
        					<Dropdown.Item onClick={(e)=>selectIncome(e)}>Income</Dropdown.Item>
        					<Dropdown.Item onClick={(e)=>selectExpense(e)}>Expense</Dropdown.Item>
        								
      				</DropdownButton>
		 		
		 	</Form.Group>

		 	<h2 className="text-center mb-5"className="currentBalance">Current Balance: PHP {currentBalance}</h2>


		
		 

		 <Modal show={show} onHide={handleClose}>
				<Modal.Header>
					<Modal.Title className="infoText" >Add a Record</Modal.Title>					
				</Modal.Header>
				<Modal.Body>

				<Form onSubmit={(e)=> addRecord(e)}>
					<Form.Group controlId="categoryName">
						<DropdownButton variant="secondary" title="Choose a Category" alignRight>
        				<Dropdown.Header className="infoText">Categories</Dropdown.Header>
        					{	
        						records.map(record =>{
        							return(
        								<Dropdown.Item 
        								key={record._id} 
        								onClick={()=> { setCategoryName(record.name),
        									setCategoryId(record._id)}}>
        								
        								{record.name}
        								</Dropdown.Item>
        							)
        						})
        					}
      				</DropdownButton>
      				<Form.Control type='text' placeholder={categoryName} value={categoryName} required disabled />
  
						
					
					</Form.Group>
					<Form.Group controlId="type">
					<DropdownButton variant="secondary" title="Choose a Type of Transaction " alignRight>
					
        				<Dropdown.Header className="infoText">Category Type</Dropdown.Header>
        					<Dropdown.Item onClick={(e)=> income(e)}>Income</Dropdown.Item>
        					<Dropdown.Item onClick={(e)=> expense(e)}>Expense</Dropdown.Item>
					
      				</DropdownButton>
      				<Form.Control type='text' placeholder={type} value={type} required disabled />
      				<Form.Group controlId="amount">
						<Form.Label className="infoText">Amount:</Form.Label>
								<Form.Control type='number' placeholder="Enter Amount" value={amount} onChange={e => setAmount(e.target.value)} required />
					</Form.Group>
					<Form.Group controlId="description">
						<Form.Label className="infoText">Description:</Form.Label>
						<Form.Control type='text' placeholder="Enter Description" value={description} onChange={e => setDescription(e.target.value)} required />
					</Form.Group>
						
					</Form.Group>

					{
						isActive === true

						?
						<Button variant="secondary" type="submit" id="submitBtn">Submit</Button>
						:
						<Button variant="secondary" type="submit" id="submitBtn" disabled>Submit</Button>
					}

					
					
				</Form>
			</Modal.Body>
			</Modal>
		
			
			<Container className="cardsContainer">
			<Row>

				
				{
								targetRecord === ""
								?
								allWallet.map(record =>{
									return(
										<Fragment>
										<Card  style={{width:'15rem'}} className=" mb-3 pb-3 mx-auto text-center">
											<Card.Header className="infoText">{record.categoryName}</Card.Header>
											<Card.Title className="infoText mt-2">{record.description}</Card.Title>
											<Card.Text className="infoText"> PHP {record.amount}</Card.Text>
											<Card.Text className="infoText">{record.type}</Card.Text>
											<Card.Text className="infoText">{moment(record.recordedOn).format("MMMM Do, YYYY")}</Card.Text>
											{/*<Button variant="secondary" onClick={()=> {handleShowEdit(record._id)}}>Edit</Button>*/}
										</Card>
										</Fragment>
										)
								})
							
								
								:
									 match.map(record=>{
											return(
											<Fragment>
											<Card style={{width:'15rem'}} className=" mb-3 pb-3 mx-auto text-center">
											<Card.Header className="infoText">{record.categoryName}</Card.Header>
											<Card.Title className="infoText mt-2">{record.description}</Card.Title>
											<Card.Text className="infoText"> PHP {record.amount}</Card.Text>
											<Card.Text className="infoText">{record.type}</Card.Text>
											{/*<Button variant="secondary" onClick={()=> {handleShowEdit(record._id)}}>Edit</Button>*/}
											</Card>
											
											</Fragment>
											)
									})	
					}

					
				

			</Row>
				


			</Container>
			
				
		
			
						
					
		 <Modal show={showEdit} onHide={handleCloseEdit}>
				<Modal.Header>
					<Modal.Title>Edit a Record</Modal.Title>					
				</Modal.Header>
				<Modal.Body>

				<Form onSubmit={(e)=> editWallet(e)}>
					<Form.Group controlId="categoryName">
						<DropdownButton variant="secondary" title="Choose a Category" alignRight>
        				<Dropdown.Header>Categories</Dropdown.Header>
        					{	
        						records.map(record =>{
        							return(
        								<Dropdown.Item 
        								key={record._id} 
        								onClick={()=> { setCategoryName(record.name),
        									setCategoryId(record._id)}}>
        								
        								{record.name}
        								</Dropdown.Item>
        							)
        						})
        					}
      				</DropdownButton>
					</Form.Group>
					<Form.Group controlId="type">
					<DropdownButton variant="secondary" title="Choose a Type of Transaction " alignRight>
        				<Dropdown.Header>Category Type</Dropdown.Header>
        					<Dropdown.Item onClick={(e)=> income(e)}>Income</Dropdown.Item>
        					<Dropdown.Item onClick={(e)=> expense(e)}>Expense</Dropdown.Item>
      				</DropdownButton>
      				<Form.Group controlId="amount">
						<Form.Label>Amount:</Form.Label>
								<Form.Control type='number' placeholder="Enter Amount" value={amount} onChange={e => setAmount(e.target.value)} required />
					</Form.Group>
					<Form.Group controlId="description">
						<Form.Label>Description:</Form.Label>
						<Form.Control type='text' placeholder="Enter Description" value={description} onChange={e => setDescription(e.target.value)} required />
					</Form.Group>
						
					</Form.Group>

					{
						isActive === true

						?
						<Button variant="secondary" type="submit" id="submitBtn">Submit</Button>
						:
						<Button variant="secondary" type="submit" id="submitBtn" disabled>Submit</Button>
					}

					
					
				</Form>
			</Modal.Body>
			</Modal>
			
					
			

		</Col>
		</Row>
		</Fragment>
		)
}