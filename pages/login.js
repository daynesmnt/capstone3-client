import { useState, useContext ,useEffect,Fragment} from 'react';
import {GoogleLogin} from 'react-google-login'
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Router from 'next/router';
import Swal from 'sweetalert2';


import UserContext from '../UserContext' 

import Head from 'next/head';


export default function login(){
	// set the global user state
	const {setUser} =useContext(UserContext)
	const [isActive,setIsActive] = useState('')

	//states for input
	const [email, setEmail] =useState('')
	//states for input
	const [password, setPassword] =useState('')
	useEffect(()=>{
		if(password !== '' !== '' && email !== ''){
			setIsActive(true)

		}else{
			setIsActive(false)

		}
	},[password,email])

	function authenticate(e){

		e.preventDefault()
		fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/login`,{
			'method': 'POST',
			headers : {
				'Content-Type': 'application/json'
			},
			body:JSON.stringify({
				email:email,
				password:password
			})
		}).then(res => res.json())
		.then(data=>{
			//console.log(data)
			if(data.access){

				//store our JWT in our localStorage
				localStorage.setItem('token',data.access)

				//fetch the details of our user

				fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/details`,{
					headers: {
						'Authorization' : `Bearer ${data.access}`
					}
				}).then(res => res.json())
				.then(data => {
					//console.log(data)
						setIsActive(true)
					setUser({
						id:data._id,
					})
					//console.log('Logged in Successfuly')
					Swal.fire({
						icon:'success',
						title:'Log in Successful'
					})
					Router.push('/')

				})

			}else{
					Swal.fire({
						icon:'error',
						title:'Something went wrong'
					})
				Router.push('/login')
			}
		})

	}

		const authenticateGoogleToken= (response) => {
		//console.log(response)
			const payload = {
				method : 'POST',
				headers: {
					'Content-Type' :'application/json'
				},
				body:JSON.stringify({
					tokenId:response.tokenId,
					googleToken:response.accessToken
				})
			}

			fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/verify-google-id-token`,payload)
			.then(res => {

				return res.json()
			})
			.then(data=>{
				localStorage.setItem('token', data.access)
				fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/details`,{
					headers: {
						'Authorization' : `Bearer ${data.access}`
					}
				}).then(res => res.json())
				.then(data => {
					//console.log(data)

					setUser({
						id:data._id,
					})
					//console.log('Logged in Successfuly')
					Router.push('/')
					Swal.fire({
						icon:'success',
						title:'Log in Successful'
					})
				})
				

			})

		}





	return(
		<Fragment>
		<Head><title>Login || Vinti</title></Head>
			<h1 className="infoTitle">Login</h1>
				<Form onSubmit={(e)=> authenticate(e)} className="mb-5">
				<Form.Group controlId="emailAdd">
					<Form.Label className="infoText">Email Address</Form.Label>
					<Form.Control type='text' placeholder='Enter Email' value={email} onChange={(e) => setEmail(e.target.value)}/>
				</Form.Group>
				<Form.Group controlId="password">
					<Form.Label className="infoText">Password</Form.Label>
					<Form.Control type='password' placeholder='Enter Password' value={password} onChange={(e) => setPassword(e.target.value)}/>
				</Form.Group>
				{
				isActive
				? 	<Button variant="secondary"  type="submit">Submit</Button>
				:
					<Button variant="secondary" type="submit" disabled>Submit</Button>
				}
				</Form>
				<p className="infoText">Dont have an account yet?</p>
				<GoogleLogin
				clientId="928010259562-g260l39vcgilpp6kivma0em60st2v2qb.apps.googleusercontent.com"
				
				buttontext="Login using Google"
				cookiePilicy={'single_host_origin'}
				onSuccess={ authenticateGoogleToken }
				onFailure={ authenticateGoogleToken }

				className="w-100 text-center d-flex justify-content-center"
			/>
				
		</Fragment>
		)
}
/*
928010259562-r5tgl0jlaf9inqcka0lrjvso76bvt3m8.apps.googleusercontent.com
7YiakaELIASdNIEkfLL-O85A
*/