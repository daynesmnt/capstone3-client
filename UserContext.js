import React from 'react';

//create a context object

const UserContext = React.createContext()

//Provider Component

export const UserProvider = UserContext.Provider

export default UserContext