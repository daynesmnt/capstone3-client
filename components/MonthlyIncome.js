import {useState,useEffect} from 'react';
import { Bar } from 'react-chartjs-2';
import moment from 'moment';

import Alert from 'react-bootstrap/Alert';

export default function monthlyIncome({dataIncome,label}){
//console.log(dataIncome)
	const data= {
		labels:['January', 'February','March','April','May','June','July','August','September','October','November','December'],
		datasets:[
			{
				label:label,
				backgroundColor:"Pink",
				borderColor:'black',
				hoverBackgroundColor:'indianred',
				data:dataIncome
			}

		]
	}

	return(
		<>
		<h1 className="insightlabel">Monthly Income</h1>
		<p className="paragraph"> Your Monthly Income shows the monthly stacks that you have, the higher your bar is, the more income that you have for that month. </p>
		{
			dataIncome === null
			?
			<Alert variant="info">
				You have no category yet
			</Alert>
			:
			<Bar data={data}/>
		}

		</>
		
		)
}