import {useState,useEffect} from 'react';
import { Bar } from 'react-chartjs-2';
import moment from 'moment';

import Alert from 'react-bootstrap/Alert';

export default function monthlyExpense({dataExpense,label}){
	const data= {
		labels:['January', 'February','March','April','May','June','July','August','September','October','November','December'],
		datasets:[
			{
				label:label,
				backgroundColor:"Pink",
				borderColor:'black',
				hoverBackgroundColor:'indianred',
				data:dataExpense
			}

		]
	}
	return(
		<>
		<h1 className="insightlabel">Monthly Expenses</h1>
		<p className="paragraph"> Your Monthly Expenses shows how much you spend every month, the higher the bar is, the more you spend your money on that month </p>
		{
			dataExpense === null
			?
			<Alert variant="info">
				You have no category yet
			</Alert>
			:
			<Bar data={data}/>
		}
		</>

		)
}