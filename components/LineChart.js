import {useState,useEffect} from 'react';
import { Line } from 'react-chartjs-2';
import moment from 'moment';

import Alert from 'react-bootstrap/Alert';

export default function PieChart({dataIncome,dataExpense,label,incomeArray,expenseArray}){
//console.log(dataIncome)
//console.log("!!incomeArray:" + incomeArray)
//console.log("!!expenseArray:" + expenseArray)
	const data= {
		labels:incomeArray,
		datasets:[
			{
				label:label,
				backgroundColor:"Pink",
				borderColor:'black',
				hoverBackgroundColor:'indianred',
				data:expenseArray
			}

		]
	}

	return(
		<>
		<h1 className="insightlabel">Income Vs Expenses</h1>
		<p className="paragraph"> This chart shows your balance trend where in X-axis shows you the income amounts you have earned through out time, and the X-Axis is all your Expenses. To help you interpret this, if your line is showing almost a straight line it means that the level of your income is balanced with the level of your expense. If it shows a decline in your line graph, it means that the lower your income is, the lower your expense is. Lastly, if it shows a growing line or it is going up, it means that you spend more when you earn more. </p>
		{
			dataIncome === null
			?
			<Alert variant="info">
				You have no category yet
			</Alert>
			:
			<Line data={data}/>
		}

		</>
		
		)
}