import {useContext,Fragment} from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';

//import nextjs link
import Link from 'next/link';

import UserContext from '../UserContext';

export default function NavBar(){
//	consume our usercontext to get the globaluser state

	const {user} = useContext(UserContext)
	return (
		<Navbar className="navbar" expand='lg'>
			<Link href='/'>
				<a className="navbar-brand">Vinti</a>
			</Link>
		<Navbar.Toggle aria-controls='basic-navbar-nav'/>

		<Navbar.Collapse id='basic-navbar-nav'>
			<Nav className=' ml-auto '>
			{
				user.id !== null
				?
			
				<Fragment>
				<Link href='/categories'>
					<a className="nav-link">Categories</a>
				</Link>
				<Link href='/records'>
					<a className="nav-link">Records</a>
				</Link>	
				<Link href='/insights'>
					<a className="nav-link">Insights</a>
				</Link>	
				<Link href='/logout'>
					<a className="nav-link">Logout</a>
				</Link>
				</Fragment>
				:
					<Fragment>
					<Link href='/login'>
						<a className="nav-link">Login</a>
					</Link>
					<Link href='/register'>
						<a className="nav-link">Register</a>
					</Link>
				</Fragment>
					
			}
					
				
					
			</Nav>		
		</Navbar.Collapse>
		
		</Navbar>
	)

}
